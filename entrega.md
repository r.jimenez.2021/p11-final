# ENTREGA CONVOCATORIA ENERO
Raúl Jiménez Luis r.jimenez.2021@alumnos.urjc.es
## Parte básica
He encontrado un fallo al realizar las capturas del escenario 2 y es que cuando abro el navegador por segunda vez en 
el mismo front la lista de videos se duplica en el html. Su solución no es muy compleja, pero debido a que me he 
encontrado el error en ultima instancia de la entrega, no he podido arreglarlo.

Todo lo demas a priori funciona bien segun las ejecuciones que pide la parte básica

## Parte adicional
* Información adicional para los ficheros
Cuando los streamers se registran, envian al servidor de señalización información adicional. En este caso, incluye un
título y una breve describcion en texto del fichero. Esta información se almacena en el directorio del servidor de 
señalización, y se la pasa al servidor frontal cuando éste le pida el listado, para poder mostrársela a los navegadores.

Su ejecución es igual que la de la parte básica, es decir, como viene en el enunciado

Video enlace de YouTube:
https://youtu.be/6_E5NKyroSs